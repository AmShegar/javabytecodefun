# README #

This is test project for Java bytecode manipulation.

At the moment int contains:
* Instantiate object without calling class constructor

In Java bytecode object creation and object initialization are 2 different operators. This test was made made to demonstrate this.

Class `NoConstructorClassGenerator` generate class without constructor at all. Class `SerializationConstructorGenerator` generate class witch could instantiate class without constructor using same techniques as `sun.reflect.ReflectionFactory.newConstructorForSerialization()`.

The main idea to invoke NEW and after that invoke `java.lang.Object.<init>` or did not invoke `<init>` at all. But if we try to do this straightforward we will get `VerifyError` during class loading. JVM check that we call `<init>` after NEW and that we call `<init>` of the correct class. To avoid `VerifyError` we need to extend special class `sun.reflect.SerializationConstructorAccessorImpl` in that case JVM will perform more relaxed verification of bytecode.

### How to run ###

* Install JDK 1.8
* Install Maven
* run *mvn clean install*
* check tests result

### Q&A ###

**Q** I have question/suggestion.

**A** Email me vadim.mail(а)gmail.com