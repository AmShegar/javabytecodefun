package com.example.test;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.matchers.JUnitMatchers;
import org.junit.rules.ExpectedException;

import static com.example.test.Util.start;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class CreateObjectWithoutConstructorTest {
    public static final String NO_CONSTRUCTOR_CLASS               = "com.example.test.classes.NoConstructor";
    public static final String INSTANTIATION_CLASS_WITHOUT_INVOKE = "com.example.test.classes.InstantiationClassWithoutInvoke";
    public static final String INSTANTIATION_CLASS_WITH_INVOKE    = "com.example.test.classes.InstantiationClassWithInvoke";
    public static final String MAGIC_INSTANTIATION_CLASS          = "com.example.test.classes.MagicInstantiationClass";

    private static final Logger logger = LogManager.getLogger();

    private static Class<?> noConstructorClass;
    private static File     testDir;
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @BeforeClass
    public static void setUpClass() throws Exception {
        testDir = new File(CreateObjectWithoutConstructorTest.class.getResource("/").toURI()).getCanonicalFile();
        logger.info("Test classes dir: {}", testDir);

        NoConstructorClassGenerator noConstructorClassGenerator = new NoConstructorClassGenerator(testDir, NO_CONSTRUCTOR_CLASS);
        noConstructorClassGenerator.generateClass();
        noConstructorClass = Class.forName(NO_CONSTRUCTOR_CLASS);
        logger.info("Class {} loaded", NO_CONSTRUCTOR_CLASS);
    }

    @Test
    public void testThatNoConstructorExist() {
        start();

        Constructor<?>[] constructors = noConstructorClass.getConstructors();
        assertTrue(constructors == null || constructors.length == 0);
        logger.info("Found 0 constructors");
    }

    @Test
    public void testInstantiationWithoutConstructorInvoke() throws Exception {
        start();

        InstantiationGenerator instantiationGenerator = new InstantiationGenerator(testDir, INSTANTIATION_CLASS_WITHOUT_INVOKE, NO_CONSTRUCTOR_CLASS);
        instantiationGenerator.setInvokeObjectConstructor(false);
        instantiationGenerator.generateClass();

        expectedException.expect(VerifyError.class);
        expectedException.expectMessage(JUnitMatchers.containsString("Expecting to find object/array on stack"));
        Class<?> instantiationClass = Class.forName(INSTANTIATION_CLASS_WITHOUT_INVOKE);
        logger.info("Class {} loaded", instantiationClass);
    }

    @Test
    public void testInstantiationWithObjectConstructorInvoke() throws Exception {
        start();

        InstantiationGenerator instantiationGenerator = new InstantiationGenerator(testDir, INSTANTIATION_CLASS_WITH_INVOKE, NO_CONSTRUCTOR_CLASS);
        instantiationGenerator.setInvokeObjectConstructor(true);
        instantiationGenerator.generateClass();

        expectedException.expect(VerifyError.class);
        expectedException.expectMessage(JUnitMatchers.containsString("Call to wrong initialization method"));
        Class<?> instantiationClass = Class.forName(INSTANTIATION_CLASS_WITH_INVOKE);
        logger.info("Class {} loaded", instantiationClass);
    }

    @Test
    public void testMagicInstantiation() throws Exception {
        start();

        SerializationConstructorGenerator serializationConstructorGenerator = new SerializationConstructorGenerator(testDir, MAGIC_INSTANTIATION_CLASS, NO_CONSTRUCTOR_CLASS);
        serializationConstructorGenerator.setInvokeObjectConstructor(false);
        serializationConstructorGenerator.generateClass();

        Class<?> instantiationClass = Class.forName(MAGIC_INSTANTIATION_CLASS);
        logger.info("Class {} loaded", MAGIC_INSTANTIATION_CLASS);
        Method method = instantiationClass.getDeclaredMethod(InstantiationGenerator.FACTORY_METHOD_NAME);
        assertNotNull("Factory method not found", method);

        Object invoke = method.invoke(null);
        assertNotNull("Failed to create object", invoke);
        logger.info("Object created: {}", invoke);
    }

    @Test
    public void testSerializationConstructor() throws Exception {
        start();

        Constructor<Object> objCons = Object.class.getConstructor();
        Constructor<?> c = sun.reflect.ReflectionFactory.getReflectionFactory().newConstructorForSerialization(noConstructorClass, objCons);
        Object instance = c.newInstance();
        logger.info("Instance: {}", instance);
    }
}
