package com.example.test;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Util {
    private static final Logger logger = LogManager.getLogger();

    public static void start() {
        StackTraceElement stackTrace = new Exception().getStackTrace()[1];
        String[] words = StringUtils.splitByCharacterTypeCamelCase(stackTrace.getMethodName());
        for (int i = 0; i < words.length; i++) {
            if (i == 0) {
                words[i] = StringUtils.capitalize(words[i]);
            } else {
                words[i] = words[i].toLowerCase();
            }
        }
        String testName = StringUtils.join(words, ' ');
        logger.info("");
        logger.info("========== {} ==========", testName);
    }
}
