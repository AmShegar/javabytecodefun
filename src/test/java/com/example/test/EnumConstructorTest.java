package com.example.test;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import static com.example.test.Util.start;

public class EnumConstructorTest {
    public static final String  ENUM_CLASS               = Planet.class.getName();
    public static final String  ENUM_INSTANTIATION_CLASS = "com.example.test.classes.EnumInstantiationClass";
    public static final Class[] CONSTRUCTOR_ARGS         = new Class[]{String.class, int.class, double.class, double.class};

    private static final Logger logger = LogManager.getLogger();

    private static File testDir;

    @BeforeClass
    public static void setUpClass() throws Exception {
        testDir = new File(CreateObjectWithoutConstructorTest.class.getResource("/").toURI()).getCanonicalFile();
        logger.info("Test classes dir: {}", testDir);
    }


    @Test
    public void testEnumConstructor() throws Exception {
        start();

        EnumInstantiationGenerator generator = new EnumInstantiationGenerator(testDir, ENUM_INSTANTIATION_CLASS, ENUM_CLASS, CONSTRUCTOR_ARGS);
        generator.generateClass();

        Class<?> instantiationClass = Class.forName(ENUM_INSTANTIATION_CLASS);
        Method method = instantiationClass.getDeclaredMethod(InstantiationGenerator.FACTORY_METHOD_NAME, CONSTRUCTOR_ARGS);
        Object pluto = method.invoke(null, "PLUTO", 8, 1.305e+22, 1.195e+6);
        logger.info("Pluto: {}", pluto);
        logger.info("Planets: {}", Arrays.asList(Planet.values()));
    }
}
