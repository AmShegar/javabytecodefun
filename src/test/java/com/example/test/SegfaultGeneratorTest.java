package com.example.test;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

public class SegfaultGeneratorTest {
    private static final String SEGFAULT_CLASS_NAME = "com.example.test.classes.Segfault";

    private static File testDir;

    @BeforeClass
    public static void setUpClass() throws Exception {
        testDir = new File(CreateObjectWithoutConstructorTest.class.getResource("/").toURI()).getCanonicalFile();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void segfaultTest() throws Exception {
        SegfaultGenerator generator = new SegfaultGenerator(testDir, SEGFAULT_CLASS_NAME);
        generator.generateClass();

        Class<? extends Runnable> segfaultClass = (Class<? extends Runnable>) Class.forName(SEGFAULT_CLASS_NAME);
        Runnable runnable = segfaultClass.newInstance();
        runnable.run();
        fail("Expected JVM crash");
    }
}