package com.example.test;

import java.io.File;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;

import static org.objectweb.asm.Opcodes.ACC_PUBLIC;
import static org.objectweb.asm.Opcodes.ACC_STATIC;
import static org.objectweb.asm.Opcodes.ARETURN;
import static org.objectweb.asm.Opcodes.DUP;
import static org.objectweb.asm.Opcodes.INVOKESPECIAL;
import static org.objectweb.asm.Opcodes.NEW;

public class SerializationConstructorGenerator extends InstantiationGenerator {
    public static final String MAGIC_SUPERCLASS = "sun/reflect/MagicAccessorImpl";

    public SerializationConstructorGenerator(File baseDir, String className, String classToInstantiate) {
        super(baseDir, className, MAGIC_SUPERCLASS, classToInstantiate);
    }

    @Override
    protected void generateInstantiateMethod(ClassVisitor classVisitor) {
        MethodVisitor methodVisitor = classVisitor.visitMethod(ACC_PUBLIC | ACC_STATIC,
                                                               FACTORY_METHOD_NAME,
                                                               Type.getMethodDescriptor(Type.getObjectType(classToInstantiateInternalName)),
                                                               null,
                                                               new String[]{Type.getInternalName(Throwable.class)});
        methodVisitor.visitCode();

        methodVisitor.visitTypeInsn(NEW, classToInstantiateInternalName);
        if (invokeObjectConstructor) {
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitMethodInsn(INVOKESPECIAL, Type.getInternalName(Object.class), "<init>", Type.getMethodDescriptor(Type.VOID_TYPE), false);
        }
        methodVisitor.visitInsn(ARETURN);

        methodVisitor.visitEnd();
    }
}
