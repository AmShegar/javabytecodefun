package com.example.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;

import java.io.File;

import static com.example.test.SerializationConstructorGenerator.MAGIC_SUPERCLASS;
import static org.objectweb.asm.Opcodes.*;

public class SegfaultGenerator extends ClassGeneratorBase {
    private static final Logger logger = LogManager.getLogger();

    public SegfaultGenerator(File baseDir, String className) {
        super(baseDir, className, MAGIC_SUPERCLASS);
        setImplementedInterfaces(Runnable.class);
    }

    @Override
    protected int getClassVisitorFlags() {
        return 0;
    }

    @Override
    protected void generateClass(ClassVisitor classVisitor) {
        generateDefaultConstructor(classVisitor);

        generateRunMethod(classVisitor);
    }

    private void generateRunMethod(ClassVisitor classVisitor) {
        MethodVisitor runMethod = classVisitor.visitMethod(ACC_PUBLIC,
                                                           "run",
                                                           Type.getMethodDescriptor(Type.VOID_TYPE),
                                                           null,
                                                           null);
        runMethod.visitCode();

        runMethod.visitInsn(RET);

        runMethod.visitMaxs(1, 1);
        runMethod.visitEnd();
    }
}
