package com.example.test;

import java.io.File;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;

import static org.objectweb.asm.Opcodes.ACC_PUBLIC;
import static org.objectweb.asm.Opcodes.ACC_STATIC;
import static org.objectweb.asm.Opcodes.ARETURN;
import static org.objectweb.asm.Opcodes.DUP;
import static org.objectweb.asm.Opcodes.ILOAD;
import static org.objectweb.asm.Opcodes.INVOKESPECIAL;
import static org.objectweb.asm.Opcodes.NEW;

public class EnumInstantiationGenerator extends InstantiationGenerator {
    private Class[] constructorParams = new Class[0];

    public EnumInstantiationGenerator(File baseDir, String className, String classToInstantiate, Class... constructorParams) {
        super(baseDir, className, SerializationConstructorGenerator.MAGIC_SUPERCLASS, classToInstantiate);
        setConstructorParam(constructorParams);
    }

    public void setConstructorParam(Class... constructorParams) {
        this.constructorParams = constructorParams;
    }

    public Class[] getConstructorParam() {
        return constructorParams;
    }

    @Override
    protected void generateInstantiateMethod(ClassVisitor classVisitor) {
        Type[] argumentTypes = class2type(constructorParams);
        MethodVisitor methodVisitor = classVisitor.visitMethod(ACC_PUBLIC | ACC_STATIC,
                                                               FACTORY_METHOD_NAME,
                                                               Type.getMethodDescriptor(Type.getObjectType(classToInstantiateInternalName), argumentTypes),
                                                               null,
                                                               new String[]{Type.getInternalName(Throwable.class)});
        methodVisitor.visitCode();

        methodVisitor.visitTypeInsn(NEW, classToInstantiateInternalName);
        methodVisitor.visitInsn(DUP);
        pushArgsToStack(methodVisitor, argumentTypes);
        methodVisitor.visitMethodInsn(INVOKESPECIAL, classToInstantiateInternalName, "<init>", Type.getMethodDescriptor(Type.VOID_TYPE, argumentTypes), false);
        methodVisitor.visitInsn(ARETURN);

        methodVisitor.visitMaxs(0, 0);

        methodVisitor.visitEnd();
    }


    private void pushArgsToStack(MethodVisitor methodVisitor, Type[] argumentTypes) {
        if (argumentTypes == null || argumentTypes.length == 0) {
            return;
        }

        for (int i = 0; i < argumentTypes.length; i++) {
            int opcode = argumentTypes[i].getOpcode(ILOAD);
            methodVisitor.visitIntInsn(opcode, i);
        }
    }
}
