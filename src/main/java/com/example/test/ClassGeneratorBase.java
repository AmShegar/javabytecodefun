package com.example.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;

import static org.objectweb.asm.Opcodes.ACC_PUBLIC;
import static org.objectweb.asm.Opcodes.ALOAD;
import static org.objectweb.asm.Opcodes.INVOKESPECIAL;
import static org.objectweb.asm.Opcodes.RETURN;
import static org.objectweb.asm.Opcodes.V1_5;

public abstract class ClassGeneratorBase {
    private static final Pattern CLASS_NAME_PATTER = Pattern.compile("([\\p{L}_$][\\p{L}\\p{N}_$]*\\.)*[\\p{L}_$][\\p{L}\\p{N}_$]*");
    private static final Logger  logger            = LogManager.getLogger();

    protected File     baseDir;
    protected String   className;
    protected String   superClassName;
    protected String[] implementedInterfaces;

    public ClassGeneratorBase(File baseDir, String className) {
        this(baseDir, className, Type.getInternalName(Object.class), null);
    }

    public ClassGeneratorBase(File baseDir, String className, String superClassName) {
        this(baseDir, className, superClassName, null);
    }

    public ClassGeneratorBase(File baseDir, String className, String superClassName, String[] implementedInterfaces) {
        setBaseDir(baseDir);
        setClassName(className);
        setSuperClassName(superClassName);
        setImplementedInterfaces(implementedInterfaces);
    }

    public String getSuperClassName() {
        return superClassName;
    }

    public String[] getImplementedInterfaces() {
        return implementedInterfaces;
    }

    public void setImplementedInterfaces(Class<?>... implementedInterfaces) {
        if (implementedInterfaces == null || implementedInterfaces.length == 0) {
            setImplementedInterfaces((String[]) null);
        } else {
            String[] interfaces = new String[implementedInterfaces.length];
            for (int i = 0; i < implementedInterfaces.length; i++) {
                interfaces[i] = Type.getInternalName(implementedInterfaces[i]);
            }
            setImplementedInterfaces(interfaces);
        }
    }

    public void setImplementedInterfaces(String... implementedInterfaces) {
        this.implementedInterfaces = implementedInterfaces;
    }

    public void addImplementedInterface(String implementedInterface) {
        implementedInterfaces = ArrayUtils.add(implementedInterfaces,
                                               implementedInterfaces == null ? 0 : implementedInterfaces.length,
                                               implementedInterface);
    }

    public void setSuperClassName(String superClassName) {
        if (superClassName == null || superClassName.isEmpty()) {
            throw new IllegalArgumentException("Super class name could not be empty");
        }
        this.superClassName = superClassName;
    }

    public File getBaseDir() {
        return baseDir;
    }

    public void setBaseDir(File baseDir) {
        if (!baseDir.exists()) {
            throw new IllegalArgumentException("Directory " + baseDir + " does not exist");
        }
        if (!baseDir.isDirectory()) {
            throw new IllegalArgumentException(baseDir + " not a directory");
        }
        logger.debug("Set basic dir to: {}", baseDir);
        this.baseDir = baseDir;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        logger.debug("Set class name: {}", className);
        this.className = validateClassName(className);
    }

    protected static String validateClassName(String className) {
        if (className == null || className.isEmpty()) {
            throw new NullPointerException("ClassName could not bu null or empty");
        }
        if (!CLASS_NAME_PATTER.matcher(className).matches()) {
            throw new IllegalArgumentException("Invalid class name: '" + className + "'");
        }
        return className;
    }

    public void generateClass() throws Exception {
        logger.info("Start {} class generation", className);
        ClassWriter classVisitor = new ClassWriter(getClassVisitorFlags());
        classVisitor.visit(V1_5,
                           ACC_PUBLIC,
                           className.replace('.', '/'),
                           null,
                           superClassName,
                           implementedInterfaces);
        generateClass(classVisitor);
        classVisitor.visitEnd();
        logger.info("Class {} generated", className);

        File classFile = getClassFile();
        FileOutputStream out = openFileOutputStream(classFile);
        out.write(classVisitor.toByteArray());
        out.close();
        logger.info("Class {} written to {}", className, classFile);
    }

    protected int getClassVisitorFlags() {
        return ClassWriter.COMPUTE_MAXS | ClassWriter.COMPUTE_FRAMES;
    }

    protected abstract void generateClass(ClassVisitor classVisitor) throws Exception;

    protected FileOutputStream openFileOutputStream(File classFile) throws IOException {
        File parentDir = classFile.getParentFile();
        if (!parentDir.exists() && !parentDir.mkdirs()) {
            throw new IOException("Failed to create dir: " + parentDir);
        }
        return new FileOutputStream(classFile);
    }

    protected File getClassFile() throws IOException {
        return new File(getBaseDir(), getClassName().replace('.', '/') + ".class").getCanonicalFile();
    }

    protected void generateDefaultConstructor(ClassVisitor classVisitor) {
        logger.debug("Generating default constructor for {}", className);
        MethodVisitor methodVisitor = generateConstructorStart(classVisitor);
        methodVisitor.visitCode();

        methodVisitor.visitVarInsn(ALOAD, 0);
        methodVisitor.visitMethodInsn(INVOKESPECIAL, Type.getInternalName(Object.class), "<init>", Type.getMethodDescriptor(Type.VOID_TYPE), false);
        methodVisitor.visitInsn(RETURN);
        methodVisitor.visitMaxs(1, 1);

        methodVisitor.visitEnd();
    }

    protected MethodVisitor generateConstructorStart(ClassVisitor classVisitor, Class... args) {
        return classVisitor.visitMethod(ACC_PUBLIC,
                                        "<init>",
                                        Type.getMethodDescriptor(Type.VOID_TYPE, class2type(args)),
                                        null,
                                        null);
    }

    protected static Type[] class2type(Class... classes) {
        if (classes == null) {
            return null;
        }
        if (classes.length == 0) {
            return new Type[0];
        }

        Type[] types = new Type[classes.length];
        for (int i = 0; i < classes.length; i++) {
            types[i] = Type.getType(classes[i]);
        }
        return types;
    }
}
